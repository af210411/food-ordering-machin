import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodGUI {
    private JPanel root;
    private JButton tempraButton;
    private JButton ramenButton;
    private JButton udonButton;
    private JTextArea receivedInfo;
    private JButton sobaButton;
    private JButton curryButton;
    private JButton sushiButton;
    private JTextPane itemList;
    private JButton checkOutButton;
    private JLabel totalPrice;

    int total=0;

    void buttonAction(String food,int price){
        int confirmation=JOptionPane.showConfirmDialog (null, "Would you like to order "+food+"?", "Order Confirmation",
                JOptionPane.YES_NO_OPTION);

        if (confirmation==0) {
            //itemList.setText(food+" "+price+" yen");
            JOptionPane.showMessageDialog(null, "Order for " + food + " received.");
            String currentText = itemList.getText();
            itemList.setText(currentText + food + " " + price + " yen" + "\n");

            total+=price;
            totalPrice.setText(total+" yen");
        }
    }

    public FoodGUI() {

        tempraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                buttonAction("Tempra",500);
            }
        });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                buttonAction("Ramen",650);
            }
        });
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                buttonAction("Udon",600);
            }
        });
        sobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                buttonAction("Soba",550);
            }
        });
        curryButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                buttonAction("Curry",700);
            }
        });
        sushiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                buttonAction("Sushi",900);
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int checkOutConfirmation=JOptionPane.showConfirmDialog (null, "Would you like to check out?", "Check out Confirmation",
                        JOptionPane.YES_NO_OPTION);

                if(checkOutConfirmation==0){
                    if(total==0){
                        JOptionPane.showMessageDialog(null,"No food is ordered. Please choose food from menu.");
                    }
                    else {
                        JOptionPane.showMessageDialog(null,"Thank you! The total price is "+total+" yen.");
                        itemList.setText("");
                        total=0;
                        totalPrice.setText(total+" yen");
                    }

                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodGUI");
        frame.setContentPane(new FoodGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }


}
